;;;; scratchnote.el
;;; this code requires popwin.el

;;; code:
(defvar scratchnote-temprate "#+TITLE: scratchnote\n")

(defvar scratchnote-hook (list (lambda ()
                                 (org-mode))))

(defvar scratchnote-filename (expand-file-name "~/scratchnote.org"))

(defvar scratchnote-buffername "*scratchnote*")

(defvar scratchnote-buffer nil)

(defvar scratchnote-dont-save nil)

(defun scratchnote--buffer-init (&optional after)
  (setf scratchnote-buffer (generate-new-buffer scratchnote-buffername))
  (with-current-buffer scratchnote-buffer
    (dolist (hook scratchnote-hook)
      (funcall hook))
    (dolist (p after)
      (funcall p))))

(defun scratchnote (&optional no-switch)
  (interactive)
  (if (buffer-live-p scratchnote-buffer)
      (switch-to-buffer scratchnote-buffer)
    (scratchnote--buffer-init
     (list (lambda ()
             (insert scratchnote-temprate)))))
  (unless no-switch
    (switch-to-buffer scratchnote-buffer)))

(defun scratchnote-save ()
  (interactive)
  (if (not (buffer-live-p scratchnote-buffer))
      (message "scratchnote buffer is not living!")
    (if (not scratchnote-dont-save)
        (with-current-buffer scratchnote-buffer
          (write-file scratchnote-filename))
      (message "scratchnote is not saved because scratchnote-dont-save flag is ON"))))

(defun scratchnote-restore ()
  (interactive)
  (when (file-exists-p scratchnote-filename)
    (unless (buffer-live-p scratchnote-buffer)
      (scratchnote--buffer-init))
    (with-current-buffer scratchnote-buffer
      (erase-buffer)
      (insert (with-temp-buffer
                (insert-file-contents scratchnote-filename)
                (buffer-substring-no-properties (point-min) (point-max)))))))

(defun scratchnote-pop ()
  (interactive)
  (if (buffer-live-p scratchnote-buffer)
      (popwin:popup-buffer scratchnote-buffer)
    (scratchnote t)
    (popwin:popup-buffer scratchnote-buffer)))


(defun scratchnote-dont-save ()
  (interactive)
  (setf scratchnote-dont-save t))

(defun scratchnote-do-save ()
  (interactive)
  (setf scratchnote-dont-save nil))

(defun scratchnote-clear ()
  (interactive)
  (with-current-buffer scratchnote-buffer
    (erase-buffer)
    (insert scratchnote-temprate)))

(provide 'scratchnote)
;;; ends here
