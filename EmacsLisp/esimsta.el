;;; esimsta.el

;; code:

(defvar esimsta::version "0.2")

(defvar esimsta::gnuplot "gnuplot")
(defvar esimsta::gnuplot-common "set png notransparent size 1000,750 font \"Times New Roman,24\"")

(defun esimsta::reduce (func seq)
  (let ((acum (elt seq 0))
        (next (elt seq 1))
        (next-seq-point 2))
    (while (<= next-seq-point (length seq))
      (setq acum (funcall func acum next))
      (setq next (elt seq  next-seq-point))
      (setq next-seq-point (+ next-seq-point 1)))
    acum))

(defun esimsta::flength (seq)
  (float (length seq)))

(defun esimsta::combine-seq (func seq1 seq2)
  (if (= (length seq1) (length seq2))
      (let ((n -1))
        (mapcar (lambda (x)
                  (setq n (+ n 1))
                  (funcall func x (elt seq2 n)))
                seq1))
    (error "sequence length mismatch.")))

(defun esimsta::find-if (pred seq)
  (let (result
        break
        (n 0))
    (while (and (null break) (< n (length seq)))
      (when (funcall pred (elt seq n))
        (setq result (elt seq n))
        (setq break t))
      (setq n (+ 1 n)))
    result))

(defun esimsta::find (item seq &optional test)
  (let ((test (if (null test) #'eq test)))
    (esimsta::find-if (lambda (seq-item)
                        (funcall test item seq-item))
                      seq)))

(defun esimsta:oddp (n)
  (/= (% n 2) 0))

(defun esimsta::nth-root (x n)
  (expt x (/ 1.0 n)))

(defun esimsta:load-data-from-simple-file (filename separator &optional ignore-line-marks)
  (with-temp-buffer
    (insert-file-contents filename)
    (goto-char 0)
    (let (result
          (n 0)
          (lines (count-lines (point-min) (point-max))))
      (while (< n lines)
        (let ((line (buffer-substring-no-properties (point-at-bol) (point-at-eol))))
          (when (or (not ignore-line-marks) (not (esimsta::find (elt line 0) ignore-line-marks)))
            (setq result (append result (list (mapcar #'string-to-number (split-string line separator))))))
          (setq n (+ 1 n))
          (forward-line 1)))
      result)))

(defun esimsta:extract-column (loaded-data n)
  (mapcar (lambda (column) (elt column n)) loaded-data))

(defun esimsta:sum (seq)
  (esimsta::reduce #'+ seq))

(defun esimsta:arithmetic-mean (seq)
  (/ (esimsta:sum seq) (esimsta::flength seq)))

(defun esimsta:geometric-mean (seq)
  (esimsta::nth-root (esimsta::reduce #'* seq) (esimsta::flength seq)))

(defun esimsta:harmonic-mean (seq)
  (/ (esimsta::flength seq)
     (esimsta:sum (mapcar (lambda (x) (/ 1.0 x))
                          seq))))

(defun esimsta:avg (seq &optional mode)
  (let ((mode (if mode mode :A)))
    (cond
     ((or (eq mode :a) (eq mode :A)) (esimsta:arithmetic-mean seq))
     ((or (eq mode :g) (eq mode :G)) (esimsta:geometric-mean seq))
     ((or (eq mode :h) (eq mode :H)) (esimsta:harmonic-mean seq))
     (t (esimsta:arithmetic-mean seq)))))

(defun esimsta:median (seq)
  (let ((seq (sort seq #'<))
        (seq-length (length seq)))
    (if (esimsta:oddp seq-length)
        (elt seq (/ seq-length 2))
      (esimsta:avg (list (elt seq (/ seq-length 2))
                         (elt seq (+ 1 (/ seq-length 2))))))))

(defun esimsta:quantile (seq k)
  ;; そのうち
  )

(defun esimsta:deviation (seq)
  (let ((avg (esimsta:avg seq)))
    (mapcar (lambda (x)
              (- x avg))
            seq)))

(defun esimsta:variance (seq)
  (esimsta:avg (mapcar (lambda (x) (* x x))  (esimsta:deviation seq))))

(defun esimsta:unbiased-variance (seq)
  (/ (esimsta:sum (mapcar (lambda (x) (* x x))  (esimsta:deviation seq)))
     (- (length seq) 1.0)))

(defun esimsta:standard-deviation (seq)
  (sqrt (esimsta:variance seq)))

(defun esimsta:standardization (seq)
  (let ((avg (esimsta:avg seq))
        (sd (esimsta:standard-deviation seq)))
    (mapcar (lambda (x) (/ (- x avg) sd)) seq)))

(defun esimsta:coefficient-of-variation (seq)
  (/ (esimsta:avg seq) (esimsta:standard-deviation seq)))

(defun esimsta:coefficient-of-correlation (seq1 seq2)
  (let ((seq1-avg (esimsta:avg seq1))
        (seq2-avg (esimsta:avg seq2))
        (seq1-deviation (esimsta:deviation seq1))
        (seq2-deviation (esimsta:deviation seq2)))
    (/ (esimsta:sum (esimsta::combine-seq #'* seq1-deviation seq2-deviation))
       (* (sqrt (esimsta:sum (mapcar (lambda (x) (* x x)) seq1-deviation)))
          (sqrt (esimsta:sum (mapcar (lambda (x) (* x x)) seq2-deviation)))))))

(provide 'esimsta)
