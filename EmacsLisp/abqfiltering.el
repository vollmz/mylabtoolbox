;;; filtering.el for abq file
;; this script requires s.el

(defvar abqfiltering::*c-data-log* nil)
(defvar abqfiltering::*c-data* nil)
(defvar abqfiltering::*buffer-name* "Abq-filtering")
(defvar abqfiltering::op "<Abq data filtering>\n")
(defvar abqfiltering::promp "---> ")
(defvar abqfiltering::promp-length (length abqfiltering::promp))
(defvar abqfiltering::result-max 5)
(defvar abqfiltering::result-log nil)


(defvar abqfilter-mode-map
  (let ((keys (make-keymap)))
    (define-key keys (kbd "RET") 'abqfiltering::run-command)
    keys))

(defun abqfilter-mode ()
  (interactive)
  (kill-all-local-variables)
  (use-local-map abqfilter-mode-map)
  (setq mode-name "abqfilter")
  (setq major-mode 'abqfilter-mode)
  (run-hooks 'abqfilter-mode-hook))

(defun abqfiltering::chipspaces (str)
  (s-replace " " "" str))

(defun abqfiltering::insert-readonly-next-line ()
  (let ((inhibit-read-only t))
    (insert (propertize "\n" 'read-only t 'rear-nonsticky t))))

(defun abqfiltering::read-coordinates (lstr)
  (let* ((splited-lstr (split-string lstr ","))
         (read-splited-lstr (mapcar #'read-from-string splited-lstr)))
    (mapcar (lambda (data)
              (let ((body (first data)))
                (when (numberp body)
                  body))) read-splited-lstr)))

(defun abqfiltering::nil-in-list-p (lst)
  (numberp (position nil lst)))

(defun abqfiltering::get-current-line ()
  (buffer-substring-no-properties (point-at-bol)
                                  (point-at-eol)))

(defun abqfiltering::filter (fn lst)
  (let (rtn)
    (dolist (e lst)
      (when (funcall fn e)
        (push e rtn)))
    (nreverse rtn)))

(defun abqfiltering::write-string-to-file (file-name str)
  (with-temp-buffer
    (insert str)
    (write-file file-name)))

(defun abqfiltering::load-data (str)
  (let* (rtn
         err
         rd-on
         (chiped-str (abqfiltering::chipspaces str))
         (line-strings (split-string chiped-str "\n" t)))
    (dolist (lstr line-strings)
      (when (s-matches? "^\\*NODE," lstr)
        (setf rd-on t))
      (when (s-matches? "^\\*ELEMENT," lstr)
        (setf rd-on nil))
      (when (and rd-on (not (s-matches? "^\\*" lstr)))
        (let ((lis-c-data (abqfiltering::read-coordinates lstr)))
          (when (abqfiltering::nil-in-list-p lis-c-data)
            (setf err t))
          (push lis-c-data rtn))))
    (list err (nreverse rtn))))

(defun abqfiltering::start-abq-filtering-2 (data)
  (let ((working-buffer (generate-new-buffer abqfiltering::*buffer-name*)))
    (switch-to-buffer working-buffer)
    (insert abqfiltering::op)
    (insert "\n")
    (abqfilter-mode)
    (make-local-variable 'abqfiltering::*c-data*)
    (make-local-variable 'abqfiltering::*c-data-log*)
    (make-local-variable 'abqfiltering::result-log)
    (setf abqfiltering::*c-data* data)
    (push abqfiltering::*c-data-log* data)
    (abqfiltering::input-start)))

(defun abqfiltering:start-abq-filtering ()
  (interactive)
  (let* ((loaded-data (abqfiltering::load-data (buffer-string)))
         (body-data (second loaded-data))
         (err-data (first loaded-data)))
    (when err-data
      (message "Some data was NOT load correctly."))
    (abqfiltering::start-abq-filtering-2 body-data)))

(defun abqfiltering::input-start ()
  (let ((clin (abqfiltering::get-current-line))
        (inhibit-read-only t))
    (abqfiltering::insert-readonly-next-line)
    (insert (propertize abqfiltering::promp 'read-only t 'rear-nonsticky t 'front-sticky t))))

(defun abqfiltering::run-command ()
  (interactive)
  (let* ((line-str (abqfiltering::get-current-line))
         (cmd (s-collapse-whitespace (s-trim (s-right (- (length line-str)
                                                         abqfiltering::promp-length)
                                                      line-str))))
         (cmd-splited (s-split " " cmd))
         (cmd-sym (intern (s-concat "abqfiltering:command-" (first cmd-splited))))
         (cmd-args (second (symbol-function cmd-sym)))
         (cmd-args-length (length cmd-args)))
    (insert "\n")
    (if (null (symbol-function cmd-sym))
        (insert "Command not found.")
      (if (= cmd-args-length (length (rest cmd-splited)))
          (let  ((result (apply cmd-sym (rest cmd-splited))))
            (abqfiltering::save-result result)
            (insert result))
        (insert "Invalid arguments."))))
  (abqfiltering::input-start))

(defun abqfiltering::right-of-promp-p ()
  (let ((point-vs-bol (- (point) (point-at-bol))))
    (< abqfiltering::promp-length point-vs-bol)))

(defun abqfiltering::save-result (result)
  (let ((tlist (append abqfiltering::result-log (list result))))
    (setf abqfiltering::result-log
          (if (> (length tlist) abqfiltering::result-max)
              (subseq tlist (- (length tlist) abqfiltering::result-max) (length tlist))
            tlist))))

(defun abqfiltering::update-c-data (newdata)
  (push abqfiltering::*c-data* abqfiltering::*c-data-log*)
  (setf abqfiltering::*c-data* newdata))

;; --- Commands ---

(defun abqfiltering:command-ls ()
  (let ((rtn ""))
    (dolist (d abqfiltering::*c-data*)
      (setf rtn (s-concat rtn (format "%s,%s,%s,%s\n"
                                      (nth 0 d)
                                      (nth 1 d)
                                      (nth 2 d)
                                      (nth 3 d)))))
    rtn))

(defun abqfiltering:command-result (lognum)
  (nth (- (string-to-number lognum) 1) abqfiltering::result-log))

(defun abqfiltering:command-filter (target op num)
  (let ((target-num (cond ((or (s-equals? target "x")
                               (s-equals? target "X"))
                           1)
                          ((or (s-equals? target "y")
                               (s-equals? target "Y"))
                           2)
                          ((or (s-equals? target "z")
                               (s-equals? target "Z"))
                           3)
                          ((or (s-equals? target "n")
                               (s-equals? target "N"))
                           0)))
        (op-func (cond ((s-equals? op "=") '=)
                       ((s-equals? op "/=") '/=) 
                       ((s-equals? op ">") '>)
                       ((s-equals? op ">=") '>=)
                       ((s-equals? op "<") '<)
                       ((s-equals? op "<=") '<=)))
        (num (string-to-number num)))
    (if (null target-num)
        "Invalid target."
      (if (null op-func)
          "Invalid Operator"
        (let ((result-data (abqfiltering::filter (lambda (itm)
                                                   (funcall op-func (nth target-num itm) num))
                                                 abqfiltering::*c-data*)))
          (abqfiltering::update-c-data result-data)
          (abqfiltering:command-ls))))))

(defun abqfiltering:command-undo ()
  (when (/= (length abqfiltering::*c-data-log*) 0)
    (setf abqfiltering::*c-data* (first abqfiltering::*c-data-log*))
    (setf abqfiltering::*c-data-log* (rest abqfiltering::*c-data-log*)))
  (abqfiltering:command-ls))

(defun abqfiltering:command-save-current-data (file-name)
  (let ((save-str (abqfiltering:command-ls)))
    (abqfiltering::write-string-to-file file-name save-str)
    (s-concat "Saved to " file-name)))

(defun abqfiltering:command-save-current-nset (file-name)
  (let ((save-str ""))
    (dolist (d abqfiltering::*c-data*)
      (setf save-str (s-concat save-str (format "%s,\n"
                                                (nth 0 d)))))
    (abqfiltering::write-string-to-file file-name save-str)
    (s-concat "Saved to " file-name)))

(defun abqfiltering:command-exit ()
  (kill-buffer (current-buffer)))

(provide 'abqfiltering)
