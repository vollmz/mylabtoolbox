#include "config.hpp"
#include <iostream>

SldbConfig::SldbConfig(){
}

SldbConfig::SldbConfig(QString fname){
  setFilename(fname);
}

void SldbConfig::setFilename(QString fname){
  filename = fname;
}

bool SldbConfig::loadFile(){
  QFile inputfile(filename);
  if(!inputfile.open(QIODevice::ReadOnly | QIODevice::Text)){
    return false;
  }

  QTextStream in(&inputfile);
  while(!in.atEnd()){
    QString line = in.readLine();
    appendValues(line);
   }
  return true;
}

void SldbConfig::appendValues(QString l){
  if(l.length() != 0 && l.at(0) != QChar('#') && l.at(0) != QChar(';') && l.at(0) != QChar('=')){
    QStringList nameandval = l.split("=");
    QString name = nameandval[0];
    QString val;
    if(nameandval.size() > 2){
      nameandval.pop_front();
      val = nameandval.join("=");
      data[name] = val;
    }else if(nameandval.size() == 2){
      val = nameandval[1];
      data[name] = val;
    }
  }
}

QString SldbConfig::getData(QString name){
  return data[name];
}
