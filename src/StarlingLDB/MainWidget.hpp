#ifndef MAINWIDGET_HPP
#define MAINWIDGET_HPP

#include <QWidget>
#include <QWebEngineView>
#include <QListView>

class MainWidget : public QWidget {
  Q_OBJECT
public:
  MainWidget();

private:
  QWebEngineView* mainview;
  QListView* selectlist;
};

#endif
