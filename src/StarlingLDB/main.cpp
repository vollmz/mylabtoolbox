#include <QApplication>
#include <QString>
#include <QDir>
#include "config.hpp"
#include "MainWindow.hpp"

const QString configfilename = QDir::homePath() + QString("/.sldb.cfg");

int main(int argc,char **argv){
  QApplication app(argc,argv);
  app.setApplicationName("StarlingLDB");

  SldbConfig cfg(configfilename);
  cfg.loadFile();

  MainWindow* window = new MainWindow();
  window->show();

  return app.exec();
}
