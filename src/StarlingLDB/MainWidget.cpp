#include "MainWidget.hpp"

#include <QSplitter>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSizePolicy>

MainWidget::MainWidget(){
  QSplitter* uisplitter = new QSplitter(Qt::Horizontal);
  uisplitter->setStretchFactor(0,3);
  uisplitter->setStretchFactor(1,1);

  mainview = new QWebEngineView();
  mainview->load(QUrl("file:///home/vollmz/index.html"));

  selectlist = new QListView();

  QHBoxLayout* uilayout = new QHBoxLayout();
  uisplitter->addWidget(mainview);
  uisplitter->addWidget(selectlist);
  uilayout->addWidget(uisplitter);

  setLayout(uilayout);
}
