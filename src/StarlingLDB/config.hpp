#ifndef CONFIG_HPP
#define CONFIG_HPP
#include <QChar>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QMap>
#include <QFile>

class SldbConfig {
public:
  SldbConfig();
  SldbConfig(QString);

  void setFilename(QString);
  bool loadFile();

  QString getData(QString);

private:
  QString filename;
  QMap<QString,QString> data;

  void appendValues(QString);
};

#endif
