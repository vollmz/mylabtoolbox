#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>
#include <QTabWidget>
#include "MainWidget.hpp"

#define WINDOW_TITLE "StarlingLDB"

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  MainWindow();

private:
  QTabWidget* tab;
  MainWidget* widget;
};

#endif
