#include "MainWindow.hpp"

MainWindow::MainWindow(){
  widget = new MainWidget();
  setCentralWidget(widget);
  setWindowTitle(WINDOW_TITLE);
}
