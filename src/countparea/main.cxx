#include <iostream>
#include <vector>
#include <opencv/cv.hpp>
#include <opencv/highgui.h>
using namespace std;
using namespace cv;

void count_peeling(char* file){
  Mat img = imread(file, IMREAD_UNCHANGED);
  int w = img.cols;
  int h = img.rows;
  Mat gray_img;
  cvtColor(img,gray_img,CV_RGB2GRAY);
  Mat bin_img;
  //  threshold(gray_img,bin_img,0,255,THRESH_BINARY|THRESH_OTSU);
  adaptiveThreshold(gray_img,bin_img,255,CV_ADAPTIVE_THRESH_GAUSSIAN_C,CV_THRESH_BINARY,99,8);
  bin_img = ~bin_img;

  //cv::dilate(bin_img,bin_img,cv::Mat(),cv::Point(-1,-1),1);

  imwrite("bin_count.png",bin_img);

  vector<vector<Point> > contours;
  findContours(bin_img, contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

  double area = 0;

  for(int i=0; i<contours.size(); i++){
    int count=contours.at(i).size();
    area += contourArea(contours.at(i));
    double x=0; double y=0;
    for(int j=0;j<count;j++){
      x+=contours.at(i).at(j).x;
      y+=contours.at(i).at(j).y;
    }
    x/=count;
    y/=count;
    circle(img, Point(x,y),5, Scalar(0,0,255),2,4);
  }
  cout<<"peeling parts = "<<contours.size()<<endl;
  cout<<"Area = "<<round(area)<<"/"<<h*w<<" = "<<area/(h*w)*100<<"%"<<endl;
  imwrite("result_count.png",img);
  Mat mask = Mat::zeros(h,w,CV_8UC1);
  drawContours(mask,contours,-1,Scalar(255),-1);
  imwrite("result_area.png",mask);
  int wArea = countNonZero(mask);
  double rArea = (double)wArea / (h*w);
  cout<<"Area = "<<wArea<<"/"<<h*w<<" = "<<rArea * 100<<"%"<<endl;
}

int main(int argc,char **argv){
  if(argc != 2){
    cout<<"Input image file"<<endl;
    return 1;
  }

  count_peeling(argv[1]);
  return 0;
}
