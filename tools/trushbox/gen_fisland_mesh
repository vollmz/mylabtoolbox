#!/bin/sh
#|-*- mode:lisp -*-|#
#|
exec ros -Q -- $0 "$@"
|#
(progn ;;init forms
  (ros:ensure-asdf)
  ;;#+quicklisp (ql:quickload '() :silent t)
  )

(defpackage :ros.script.gen.3741643200
  (:use :cl))
(in-package :ros.script.gen.3741643200)

(defvar *tick* 0.002)

(defmacro aif (condition t-form f-form)
  `(let ((it ,condition))
     (if it
         ,t-form
         ,f-form)))

(defun find-node (point nodes)
  (remove-if (lambda (n)
               (not (equal (cadr n) point)))
             nodes))

(defun push-node (point nodes)
  (aif (find-node point nodes)
       (values nodes (car it) nil)
       (values (append (list `(,(+ 1 (length nodes)) ,point))
                       nodes)
               (+ 1 (length nodes))
               t)))

(defun x+ (tick point)
  (list (+ tick (first point))
        (second point)
        (third point)))

(defun x- (tick point)
  (x+ (- tick) point))

(defun y+ (tick point)
  (list (first point)
        (+ tick (second point))
        (third point)))

(defun y- (tick point)
  (y+ (- tick) point))

(defun z+ (tick point)
  (list (first point)
        (second point)
        (+ tick (third point))))

(defun z- (tick point)
  (z+ (- tick) point))

(defun create-box (base-point ticks &optional nodes)
  (let (element-nodes
        points)
    (push base-point points)                                                           ; 1
    (push (x+ (first ticks) base-point) points)                                         ; 2
    (push (y+ (second ticks) (x+ (first ticks) base-point)) points)                      ; 3
    (push (y+ (second ticks) base-point) points)                                        ; 4
    (push (z+ (third ticks) base-point) points)                                         ; 5
    (push (z+ (third ticks) (x+ (first ticks) base-point)) points)                       ; 6
    (push (z+ (third ticks) (y+ (second ticks) (x+ (first ticks) base-point))) points)    ; 7
    (push (z+ (third ticks) (y+ (second ticks) base-point)) points)                      ; 8
    (dolist (point (nreverse points))
      (aif (find-node point nodes)
           (progn
             (push (car (car it)) element-nodes))
           (multiple-value-bind (n pos ap)
               (push-node point nodes)
             (declare (ignore ap))
             (setf nodes n)
             (push pos element-nodes))))
    (values (nreverse element-nodes)
            (nreverse nodes))))

(defun dist-pattern (base-point floating-point ticks)
  (let ((x-tick (first ticks))
        (y-tick (second ticks)))
   (cond
     ((equal (y- y-tick (x- x-tick floating-point)) base-point) 1)
     ((equal (y- y-tick floating-point) base-point) 2)
     ((equal (y- y-tick (x+ x-tick floating-point)) base-point) 3)
     ((equal (x- x-tick floating-point) base-point) 4)
     ((equal floating-point base-point) 5)
     ((equal (x+ x-tick floating-point) base-point) 6)
     ((equal (y+ y-tick (x- x-tick floating-point)) base-point) 7)
     ((equal (y+ y-tick floating-point) base-point) 8)
     ((equal (y+ y-tick (x+ x-tick floating-point)) base-point) 9)
     (t 0))))

(defun create-sin-box-1 (p-pattern base-point floating-tick ticks)
  (let ((points (list
                 (z+ (third ticks) (y+ (second ticks) base-point))                       ;8 0
                 (z+ (third ticks) (y+ (second ticks) (x+ (first ticks) base-point)))    ;7 1
                 (z+ (third ticks) (x+ (first ticks) base-point))                        ;6 2
                 (z+ (third ticks) base-point)                                           ;5 3
                 (y+ (second ticks) base-point)                                          ;4 4
                 (y+ (second ticks) (x+ (first ticks) base-point))                       ;3 5
                 (x+ (first ticks) base-point)                                           ;2 6
                 base-point)))                                                           ;1 7
    (cond ((= p-pattern 1)
           (setf (nth 5 points) (z+ floating-tick (nth 5 points))))
          ((= p-pattern 2)
           (setf (nth 4 points) (z+ floating-tick (nth 4 points)))
           (setf (nth 5 points) (z+ floating-tick (nth 5 points))))
          ((= p-pattern 3)
           (setf (nth 4 points) (z+ floating-tick (nth 4 points))))
          ((= p-pattern 4)
           (setf (nth 6 points) (z+ floating-tick (nth 6 points)))
           (setf (nth 5 points) (z+ floating-tick (nth 5 points))))
          ((= p-pattern 5)
           (setf (nth 7 points) (z+ floating-tick (nth 7 points)))
           (setf (nth 6 points) (z+ floating-tick (nth 6 points)))
           (setf (nth 5 points) (z+ floating-tick (nth 5 points)))
           (setf (nth 4 points) (z+ floating-tick (nth 4 points))))
          ((= p-pattern 6)
           (setf (nth 7 points) (z+ floating-tick (nth 7 points)))
           (setf (nth 4 points) (z+ floating-tick (nth 4 points))))
          ((= p-pattern 7)
           (setf (nth 6 points) (z+ floating-tick (nth 6 points))))
          ((= p-pattern 8)
           (setf (nth 7 points) (z+ floating-tick (nth 7 points)))
           (setf (nth 6 points) (z+ floating-tick (nth 6 points))))
          ((= p-pattern 9)
           (setf (nth 7 points) (z+ floating-tick (nth 7 points)))))
    (format t "pattern = ~A~%------------------------~%pos = ~A~%~%" p-pattern points)
    points))

(defun create-sin-box (base-point floating-point floating-tick ticks &optional nodes)
  (let* (element-nodes
         (pattern (dist-pattern base-point floating-point ticks)) 
         (points (create-sin-box-1 pattern base-point floating-tick ticks)))
    (dolist (point (nreverse points))
      (aif (find-node point nodes)
           (progn
             (push (car (car it)) element-nodes))
           (multiple-value-bind (n pos ap)
               (push-node point nodes)
             (declare (ignore ap))
             (setf nodes n)
             (push pos element-nodes))))
    (values (nreverse element-nodes)
            (nreverse nodes))))

(defun error-check-points (xy-size xy-div si-height si-div sin-height ge-height ge-div)
  (multiple-value-bind (rxy qxy)
      (floor xy-size xy-div)
    (declare (ignore rxy))
    (multiple-value-bind (rsi qsi)
        (floor si-height si-div)
      (declare (ignore rsi))
      (multiple-value-bind (rge qge)
        (floor ge-height ge-div)
        (declare (ignore rge))
        (and (> xy-size 0)
             (>= xy-div 5)
             (oddp xy-div)
             (> si-height 0)
             (> si-div 0)
             (> sin-height 0)
             (> ge-height 0)
             (> ge-div 0)
             (= qxy 0)
             (= qsi 0)
             (= qge 0))))))

(defun build-elements (xy-size xy-div si-height si-div sin-height ge-height ge-div)
  (when (error-check-points xy-size xy-div si-height si-div sin-height ge-height ge-div)
    (let (nodes
          elements
          (xy-ticks (/ xy-size xy-div))
          (z-ticks (/ si-height si-div))
          (fpx (/ (- xy-div 1) 2)))
      ;; Si build
      (loop for z from 0 below si-height by z-ticks do
           (loop for y from 0 below xy-size by xy-ticks do
                (loop for x from 0 below xy-size by xy-ticks do
                     (let ((point (list x y z)))
                       (multiple-value-bind (e n)
                           (create-box point (list xy-ticks xy-ticks z-ticks) nodes)
                         (setf nodes n)
                         (setf elements (append elements (list e))))))))
      
    ;; SiN build
      (loop for y from 0 below xy-size by xy-ticks do
           (loop for x from 0 below xy-size by xy-ticks do
                (let ((point (list x y si-height)))
                  (multiple-value-bind (e n)
                      (create-sin-box point
                                      (list (* fpx xy-ticks) (* fpx xy-ticks) si-height)
                                      100
                                      (list xy-ticks xy-ticks sin-height)
                                      nodes)
                    (setf nodes n)
                    (setf elements (append elements (list e)))))))

    ;; Ge build
      (loop for y from 0 below xy-size by xy-ticks do
           (loop for x from 0 below xy-size by xy-ticks do
                (let ((point (list x y (+ si-height sin-height))))
                  (multiple-value-bind (e n)
                      (create-box point (list xy-ticks xy-ticks ge-height) nodes)
                    (setf nodes n)
                    (setf elements (append elements (list e)))))))
      (values nodes elements))))

(defun scale-change-1 (lst n)
  (mapcar (lambda (x) (* x n)) lst))

(defun scale-change (nodes n)
  (mapcar (lambda (x)
            (list (car x) (scale-change-1 (cadr x) n)))
          nodes))

(defun write-abq-file (filename nodes elements)
  (with-open-file (out filename :direction :output)
    (format out "*NODE, NSET=Nall~%")
    (dolist (node nodes)
      (let ((x (first (cadr node)))
            (y (second (cadr node)))
            (z (third (cadr node))))
        (format out "    ~A, ~A, ~A, ~A~%" (car node) x y z)))
    (format out "*ELEMENT, TYPE=C3D8, ELSET=Eall~%")
    (let ((counter 1))
      (dolist (elem elements)
        (format out "~A, ~A, ~A, ~A, ~A, ~A, ~A, ~A, ~A~%"
                counter
                (nth 0 elem)
                (nth 1 elem)
                (nth 2 elem)
                (nth 3 elem)
                (nth 4 elem)
                (nth 5 elem)
                (nth 6 elem)
                (nth 7 elem))
        (incf counter)))))

(defun main (&rest argv)
  (declare (ignorable argv))
  (multiple-value-bind (n e)
      (build-elements 10000000 5 525000 2 130 330 1)
    (if (not (null n))
        (let ((scaled-n (scale-change n (expt 10.0 -9))))
          ;(format t "~A~%~%~A~%~%" scaled-n e)
          (write-abq-file "all.msh" scaled-n e))
        (format t "Incorrect params~%"))))

