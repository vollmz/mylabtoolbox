Lab tool box
============

## 概要
とある大学のとあるLabでの暮らしの中で支えられたくて作ったしょうもない奴ら。

## Insatllation
適当にビルドしたりPATH通したりsite-lispに置いたりして使ってください。

## 使い方
そんなに長くないのでソースコードを読んだり適当に動かして雰囲気で使う。
気が向いていれば簡易的なヘルプくらいはついているかもしれない。
他人が使うことを想定していない。

## 改変
改変は自由にどうぞ。大体Lispでできている。
(Lispで全部作りたい人生だった)

## License
申し訳程度にMITライセンスあたりにしとくか。
ただし、一部他のコードを流用しているところがあるのでそれは除く。

Copyright 2018 Masahiro NAGATA

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
